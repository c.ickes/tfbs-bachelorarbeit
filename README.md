# TFBS Bachelorthesis

## Usage
1. Build the dockerfile in the docker directory. Use
   ```docker build -t tfclass .``` to build it.
2. Create the container and map the repository as a volume in the container. 
   This could be done by the following snippet.
   ```
   docker run -it --gpus "device=0" -v path_to_gitrepo:/home/ -v /sybig/home/cic/Dokumente/tfbs-bachelorarbeit/data/database.db:/home/data/ --name tfclass-thesis tfclass
   ```
   or via
   ```
   docker run -it --gpus "device=0" -v /sybig/home/cic/Dokumente/tfbs-bachelorarbeit/:/home/ --name tfclass-thesis tfclass
   ```
3. Execute the scripts in the src folder by using the container interactively. 
    Use `python3` to start the specific script. 
   
(use it on ceberus)
### Other_scripts
Scripts in the other_scripts directory can just be executed if they are moved directly to the src folder.
They can not access their dependencies from this folder.

## Database
The database is located directly under the user cic (Path: */sybig/home/cic/Dokumente/tfbs-bachelorarbeit/data/database.db*).

To build the database on your own use the fill_db.py, init_db.py, and the create_labels.py in the follwing way.
The methods in init_db.py have to be executed separately.

1. init_db: `init_database()`   
2. Execute the fill_db.py with the data older mapped in or outside of the docker container. 
3. Execute the create_labels.py (needs the mapping file)
4. init_db: `init_index()`, `init_view()`
5. init_db: `optimize()`
