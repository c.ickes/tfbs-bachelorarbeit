from phylop_net.algorithms import *
from database import *
from sklearn.model_selection import KFold
import tensorflow as tf
from sklearn.preprocessing import LabelBinarizer
from sklearn.metrics import log_loss, roc_auc_score
from phylop_net.preprocessing import *

# Classifies the samples according to the TFClass level.

hidden_layer_size = np.hstack((np.logspace(1, 3, num=3), np.logspace(1, 3, num=3) * 5))
hidden_layer_size.sort()
model_save_path = ""


def train_centroid(X, y, labels):
    """
    Trains the centroid. Therefore data is split into test and train sets.
    Labels has to be transformed since sklearn can not handle one-hot encodings.
    :param X: dataset
    :param y: one-hot encoded classes
    :param labels: TFClass class
    :return: result = (log loss, accuracy on test set, AUC), per-class accuracy
    """
    # Transform Input + split
    y_new = np.argwhere(y == 1)[:, 1]
    train_idx, test_idx = train_test(np.arange(X.shape[0]))
    X_train, X_test = X[train_idx], X[test_idx]
    y_train, y_test, y_test_normal = y_new[train_idx], y_new[test_idx], y[test_idx]
    labels_test = labels[test_idx]

    # Train
    cent = centroid()
    cent.fit(X_train, y_train)

    # Transform labels
    lb = LabelBinarizer()
    lb.fit(np.unique(y_new))
    y_pred = cent.predict(X_test)
    y_pred = lb.transform(y_pred)

    # generate results
    per_class_error = [cent.score(X_test[labels_test == label], y_test[labels_test == label])
                       for label in np.unique(labels_test)]
    results = [log_loss(y_test_normal, y_pred), cent.score(X_test, y_test),
               roc_auc_score(y_test_normal, y_pred, multi_class="ovo")]
    return results, per_class_error


def train_randomF(X, y, labels):
    """
    Trains the random forest. Therefore data is split into test and train sets.
    Labels has to be transformed since sklearn can not handle one-hot encodings.
    :param X: dataset
    :param y: one-hot encoded classes
    :param labels: TFClass class
    :return: result = (log loss, accuracy on test set, AUC), per-class accuracy
    """
    # Split
    train_idx, test_idx = train_test(np.arange(X.shape[0]))
    X_train, X_test = X[train_idx], X[test_idx]
    y_train, y_test = y[train_idx], y[test_idx]
    labels_test = labels[test_idx]

    # Train + predict
    rf = randomForest()
    rf.fit(X_train, y_train)
    y_pred = rf.predict(X_test)

    # generate results
    per_class_error = [rf.score(X_test[labels_test == label], y_test[labels_test == label])
                       for label in np.unique(labels_test)]

    results = [log_loss(y_test, y_pred), rf.score(X_test, y_test), roc_auc_score(y_test, y_pred, multi_class="ovo")]
    return results, per_class_error


def train_nn(X, y, labels):
    """
    Trains the Neural network. Therefore data is split into test and train sets.
    The hidden layer size is evaluated by a 3-fold-cross-validation.
    In the end the MLP is trained with 60 epochs (or earlier if the early stopping aborts the procedure).

    Stores also the model with the best performance.
    :param X: dataset
    :param y: one-hot encoded classes
    :param labels: TFClass class
    :return: result = (log loss, accuracy on test set, AUC), per-class accuracy
    """
    train_idx, test_idx = train_test(np.arange(X.shape[0]))
    X_train, X_test = X[train_idx], X[test_idx]
    y_train, y_test = y[train_idx], y[test_idx]
    labels_test = labels[test_idx]

    # crossvalidation for hiddenlayer size
    kf = KFold(3)
    _param_stats = []
    for size in hidden_layer_size:
        iter_stats = []
        for train_idx, test_idx in kf.split(X_train):
            model = network(y_train.shape[1], X_train.shape[1], size)

            model.fit(X_train[train_idx], y_train[train_idx], epochs=3, validation_split=0.2,
                      use_multiprocessing=True)
            iter_stats.append(model.evaluate(X_train[test_idx, :], y_train[test_idx, :])[0])

        _param_stats.append(np.mean(iter_stats))

    best_param = np.argmin(_param_stats)

    # Early Stopping callback
    callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3, restore_best_weights=True)

    print(f"Best param: {best_param}")
    model = network(y_train.shape[1], X_train.shape[1], hidden_layer_size[best_param])

    model.fit(X_train, y_train, epochs=60, verbose=1, validation_split=0.2,
              use_multiprocessing=True,
              batch_size=200,
              callbacks=[callback])

    # measure accuracy on each class
    per_class_error = [model.evaluate(X_test[labels_test == label],
                                      y_test[labels_test == label])[1]
                       for label in np.unique(labels_test)]

    model.save(f"{model_save_path}.h5")

    return model.evaluate(X_test, y_test), per_class_error


def log_results(results, per_class_error):
    """
    Adds the results to the specfic log file and also the per-class error.
    :param results: (log loss, accuracy, auc)
    :param per_class_error: accuracies per class
    """
    results = map(str, results)
    per_class_error = map(str, per_class_error)
    res = "\t".join(results)
    error = "\t".join(per_class_error)
    log_file_result.write(f"{res}\t{error}\n")


def start_training(X, y, labels, algorithm, definition):
    """
    Starts the training procedure for a specfic algorithm and applies also the feature selection.
    :param X: Dataset
    :param y: class labels (one-hot encoded)
    :param labels: TFClass class labels
    :param algorithm: centroid, random forest or neural network
    :param definition: title which should be added to the logfile (algorithm name)
    """
    global model_save_path
    for site, idx in feature_selektion(X):
        model_save_path = f"{model_save_path_global}_{site}"
        final_test_alg, per_class_alg = algorithm(X[:, idx], y, labels)

        final_test_alg.insert(0, definition)

        # append FE (OR)
        expected = 1 / y.shape[1]
        final_test_alg.append(final_test_alg[2] / expected)

        # append site
        final_test_alg.append(site)

        log_results(final_test_alg, per_class_alg)


def add_header_result(labels):
    """
    Adds the header to the log file.
    :param labels: Class labels for class specific accuracies.
    """
    header_result = ['algorithm', 'loss', 'accuracy', 'auc', 'fe', 'region']
    for label in np.unique(labels):
        header_result.append(label)

    log_file_result.write('\t'.join(header_result))
    log_file_result.write('\n')


def main():
    """
    Starts the training for each chosen ML algorithm.
    Dataset gets preprocessed and balanced before.
    Reading takes a lot of time.
    """
    controller = Controller(group)
    X, y, labels = controller.get_data()
    X, y, labels = balancing(X, y, labels)
    print(len(labels), X.shape[0])
    assert len(labels) == X.shape[0]
    add_header_result(labels)
    start_training(X, y, labels, train_nn, "nn")
    start_training(X, y, labels, train_centroid, "centroid")
    start_training(X, y, labels, train_randomF, "rf")


if __name__ == '__main__':
    # Change group to address the correct TFClass level
    group = f'class_filtered'
    model_save_path_global = f"../out/models/model_{group}"

    log_file_result = open(f'../out/log/result_log_{group}.tsv', 'w')
    main()
    log_file_result.close()
