from database import ClusterController
from phylop_net import Clusterer, Visualizer
import matplotlib.pyplot as plt
import numpy as np

# Performs the clustering procedure to analyze the relation between a general TFBS of a TF and the phyloP distribution
# Reading takes a lot of time.

SAVEPATH = "../out/unsorted_results"
GROUP = "superclass_filtered"
NMIN = 2
NMAX = 100

controller = ClusterController(GROUP)
X, _, _ = controller.get_data()
clusterer = Clusterer(X)

# K Means
y, scores_kmeans = clusterer.kmeans(NMIN, NMAX)
labels = np.argmax(y, axis=1)
vis = Visualizer(X, labels, SAVEPATH, title="kmeans")
vis.get_bpplot()
vis.get_pca()
vis.get_tsne()

# Spectral Clustering - Nearest Neighbor
y, scores_snn = clusterer.spectral_clustering_nn(NMIN, NMAX)
labels = np.argmax(y, axis=1)
vis = Visualizer(X, labels, SAVEPATH, title="spectral_nn")
vis.get_bpplot()
vis.get_pca()
vis.get_tsne()

# Spectral Clustering - RBF
y, scores_srbf = clusterer.spectral_clustering_rbf(NMIN, NMAX)
labels = np.argmax(y, axis=1)
vis = Visualizer(X, labels, SAVEPATH, title="spectral_rbf")
vis.get_bpplot()
vis.get_pca()
vis.get_tsne()


# plotting silhouett_scores
xx = np.arange(NMIN, NMAX+1)
fig, ax = plt.subplots(figsize=(12, 8))
plt.plot(xx, scores_kmeans, label="KM")
plt.plot(xx, scores_snn, label="SC_nn")
plt.plot(xx, scores_srbf, label="SC_rbf")
plt.legend()
plt.savefig(f"{SAVEPATH}/silhouette_scores.svg")
plt.close()



