from database import Labeler
import pandas as pd

PATH = "../data/12.2.tab_version_02_06_2013"
DATABASE = '../data/database/database.db'


def _remove_duplicates(label_dict, tfs):
    """
    Removes duplicated values between classes.
    :param label_dict: dictonary which holds classes and the corresponding TFs
    :param tfs: dictionary of TFs which should be removed
    """
    for tf, clas in tfs.items():
        if len(clas) >= 2:
            for c in clas:
                label_dict[c].remove(tf)


def _find_duplicates(label_dict):
    """
    Finds duplicated values between classes
    :param label_dict: dictonary which holds classes and the corresponding TFs
    :return: dictionary {tf: class_index} - tf indicates the duplicated tf, class_index the found classes
    """
    tfs = dict()
    for clas, tf_list in label_dict.items():
        for tf in tf_list:
            tf = str(tf)
            try:
                tfs[tf].append(clas)
                print(tf, tfs[tf])
            except Exception:
                tfs.update({tf: [clas]})
    print('Check finished')
    return tfs


def _check_dict(label_dict):
    """
    Checks if there are duplicated labels between the classes and remove them.
    :param label_dict: dictonary which holds classes and the corresponding TFs
    """
    tfs = _find_duplicates(label_dict)
    _remove_duplicates(label_dict, tfs)

    return label_dict


def _filter_less_then(label_dict, less_then):
    """
    Filters a given dictionary from classes which has less then x (specific threshold) TFs
    :param label_dict: dictionary which should be filtered
    :param less_then: threshold
    :return: New filtered dictionary
    """
    return {key: label_list for key, label_list in label_dict.items() if len(label_list) >= less_then}


def _on_superclass_level(labels):
    """
    Produces a dictionary of TFs and their classes based on the superclass level.
    :param labels: Dataframe of classes and tfs
    :return: dictionary which holds lists of tfs. A class label indicates the corresponding and is used as a key.
    """
    label_dict = dict()
    for idx, val in labels.iterrows():
        class_split = val['class'].split('.')
        superclass = class_split[0]
        tf = val['tf'].split('$')[1]
        try:
            label_list = label_dict.get(superclass)
            label_list.add(tf)
        except Exception:
            label_dict.update({superclass: {tf}})
    return label_dict


def _on_class_level(labels, startswith=""):
    """
    Produces a dictionary of TFs and their classes based on the class level.
    :param labels: Dataframe of classes and tfs
    :return: dictionary which holds lists of tfs. A class label indicates the corresponding and is used as a key.
    """
    label_dict = dict()
    for idx, val in labels.iterrows():
        if str(val['class']).startswith(startswith):
            class_split = val['class'].split('.')
            superclass = class_split[0] + '.' + class_split[1]
            tf = val['tf'].split('$')[1]
            try:
                label_list = label_dict.get(superclass)
                label_list.add(tf)
            except Exception:
                label_dict.update({superclass: {tf}})
    return label_dict


def main(less_than, percentile):
    """
    Creates the TF labels based on the TFClass hierarchy and stores several groups of them in the database.
    Available groups:
        superclass_filtered
        class_filtered
        class.n_filtered (n in [1, 8])
    :param less_than: threshold for filtering classes with less than x TFs
    :param percentile: threshold for the score (not used in this thesis)
    :return:
    """
    labeler = Labeler(DATABASE, ignore_missings=True)

    # Removing unnecessary values (NAN, ???)
    labels = pd.read_csv(PATH, sep='\t', names=["tf", "type", "class", "description", "factor", "uniprot_id"],
                         index_col=False)
    labels.dropna(inplace=True)
    labels = labels[labels['class'] != '???']

    # Preparing labels for superclass + checking if duplicates are there
    sup_label_dict = _on_superclass_level(labels)
    sup_label_dict = _check_dict(sup_label_dict)
    sup_label_dict = _filter_less_then(sup_label_dict, less_than)
    labeler.labeling(f'superclass_filtered', sup_label_dict, percentile)

    # Preparing labels for all types of classes
    for i in range(9):
        class_label_dict = _on_class_level(labels, str(i))
        class_label_dict = _check_dict(class_label_dict)
        class_label_dict = _filter_less_then(class_label_dict, less_than)
        labeler.labeling(f'class.{i}_filtered', class_label_dict, percentile)

    class_label_dict = _on_class_level(labels)
    class_label_dict = _check_dict(class_label_dict)
    class_label_dict = _filter_less_then(class_label_dict, less_than)
    labeler.labeling(f'class_filtered', class_label_dict, percentile)

    labeler.close_connection()


if __name__ == '__main__':
    main(3, 0)
