from sqlite3 import *
import numpy as np
import multiprocessing as mp


def label_to_array(label, type):
    """
    Processes a one-hot encoded array (stored as string) to a real array.
    :param label: string of the one-hot encoded class
    :param type: datatype of the new array
    :return: transformed array
    """
    return np.fromstring(label, sep=',', dtype=float).astype(type)


def label_list_to_matrix(label_list, type):
    """
    Transforms a list of one-hot encoded arrays (as string) to the specifc matrix
    :param label_list: list of one-hot encodings
    :param type: datatype of the matrix
    :return: one-hot encoded matrix (by row)
    """
    return np.array([label_to_array(label, type) for label in label_list])


# Manages selection queries to the database for a specific group (from site_groups).
# Uses multiprocessing to gather several TFs in the same time.
class Controller:
    connection = connect("../data/database.db")
    cursor = connection.cursor()

    def __init__(self, group_name):
        """
        Inits all needed values.
        Accesses the database with activated foreign keys.
        :param group_name: Name of the group which should be processed.
        """
        self.cursor.execute("PRAGMA foreign_key = ON")
        self.group_name = group_name
        self.X = None
        self.y = None
        self.labels = None

    def get_chrs(self):
        """
        Gathers all available chromosomes from the site table for the specified class.
        :return: unique list of chromosomes without the special ones (mitochondrial)
        """
        self.cursor.execute("SELECT distinct chromosome from site where site_group='{self.group_name}';")
        chrs = self.cursor.fetchall()
        return [ch[0] for ch in chrs if len(ch[0].split('_')) == 1]

    def get_tfs(self):
        """
        Gathers all available TFs from the site table for the specified class.
        :return: unique list of TFs
        """
        print("Start selecting TFs")
        tfs = []
        self.cursor.execute(f"SELECT distinct description from dataset where site_group='{self.group_name}';")
        for tf in self.cursor.fetchone():
            print(tf[0])
            tfs.append(tf)
        return tfs

    def get_data_per_tf(self, description):
        """
        Gathers and processes phylop data, one-hot encoded label and the original class for a specific TF in a group.
        Data is transformed from string to int or float.
        :param description: Name of the TF
        :return:    X - phyloP matrix
                    y - one-hot encoded matrix (by row)
                    labels - array of class labels
        """
        X, y, labels = [], [], []
        print(f'Process started: {description}')
        self.cursor.execute(
            f"SELECT label, phyloP, label_description "
            f"FROM dataset "
            f"WHERE site_group = '{self.group_name}' "
            f"AND description ='{description}'")

        data = self.cursor.fetchall()
        for result in data:
            y.append(result[0])
            X.append(result[1])
            labels.append(result[2])

        y = label_list_to_matrix(y, int)
        X = label_list_to_matrix(X, float)

        labels = np.array(labels)
        return X, y, labels

    def get_data_per_tf_list(self, descriptions):
        """
        Stacks the data for a specific list of TFs and removes missing values.
        Is needed for the multiprocessing part.
        :param descriptions: List of TFs (or iterable)
        :return:
        """
        X, y, labels = None, None, None
        for description in descriptions:
            res = self.get_data_per_tf(description)
            if X is None and y is None:
                X = res[0]
                y = res[1]
                labels = res[2]
            else:
                X = np.vstack((X, res[0]))
                y = np.vstack((y, res[1]))
                labels = np.hstack((labels, res[2]))
        # Filtering NaNs (delets any row with a nan in the matrix)
        mask = ~np.any(np.isnan(X), axis=1)
        X = X[mask]
        y = y[mask]
        labels = labels[mask]
        print(f'Process finished')

        return X, y, labels

    # second bottleneck
    def _stack_data(self, result):
        """
        Stacks the data (results) from all processes to a global storage.
        Data has only to be gathered ones.
        :param result: Results of a process (X, y, labels)
        """
        if self.X is None and self.y is None:
            self.y = result[1]
            self.X = result[0]
            self.labels = result[2]
        else:
            self.y = np.vstack((self.y, result[1]))
            self.X = np.vstack((self.X, result[0]))
            self.labels = np.hstack((self.labels, result[2]))

    def get_data(self):
        """
        Gathers all available data (phylop) for the specified class.
        If the method is started more than once, the data will just be returned.
        1. Getting all TFs
        2. Builds a process pool and gives each process a list of TFs to process.
        3. Return the results asynchronously and stack it + store is globaly.
        :return:    X - phyloP matrix
                    y - one-hot encoded matrix (by row)
                    labels - array of class labels
        """
        if self.X is None or self.y is None:
            tfs = self.get_tfs()
            processes = int(mp.cpu_count() / 2)
            tfs = np.array_split(tfs, processes)
            pool = mp.Pool(processes=processes)

            for result in pool.imap_unordered(self.get_data_per_tf_list, tfs):
                self._stack_data(result)

            pool.close()
            pool.join()

            print('Reading finished!')
        return self.X, self.y, self.labels

    def close_connection(self):
        """
        Closes the connection to the database.
        """
        self.connection.close()


