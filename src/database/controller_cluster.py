from .controller import *

# Adapts the Controller class to calculate the mean phylop distribution per TF
class ClusterController(Controller):

    def get_data_per_tf(self, description):
        """
        Calculates the mean phylop distribution of a TF
        :param description: Transcription Factor
        :return:    X - vector containing phylop per bp
                    y - label as one-hot encoded vector
                    label - corresponding class in the TFClass hierachy
        """

        X, y, labels = super(ClusterController, self).get_data_per_tf(description)
        X = np.nanmean(X, axis=0)
        y = np.unique(y, axis=0)
        labels = np.unique(labels)
        return X, y, labels