import sqlite3 as sq
import pandas as pd
from os.path import join, isfile
import os

# Access to the database.
conn = sq.connect('../../data/database/database.db')
c = conn.cursor()
c.execute("PRAGMA foreign_key = ON")

# Defining columns for reading and inserting into the specific table.
DF_COLUMNS = ['chr', 'start', 'end', 'gen', 'plus', 'assembly', 'seq', 'file', 'score', 'strand', 'peak', 'binding',
           'phyloP']

SITE_COLUMNS = "\'description\',\'chromosome\',\'start\',\'end\',\'phylop\'"
SITE_INSERT = ['chr', 'start', 'end', 'phyloP']

BINDING_COLUMNS = "\'id\',\'sequence\',\'chromosome\',\'gen\', \'score\', \'start\',\'end\'"
BINDING_INSERT = ['seq', 'chr', 'gen', 'score', 'peak', 'binding']


def get_files(folder):
    """
    Gathers all filenames (+ relative path) from a specific folder.
    :param folder: Corresponding path to the directory.
    :return: List of files in the directory
    """
    return [join(folder, f) for f in os.listdir(folder) if isfile(join(folder, f))]


def get_amount_of_columns(path, sep):
    """
    Counts the columns of a file by using a specified separator.
    :param path: Path to the file.
    :param sep: Column separator of the file.
    :return: Amount of columns.
    """
    with open(path) as file:
        cols = len(file.readline().split(sep))
    return cols


def get_phylop_as_df(file, cols):
    """
    Converts a file of TFBS into a Dataframe. Inserts None values for missing columns.
    :param file: Path to the TFBS file.
    :param cols: Column names
    :return: File as Dataframe.
    """
    use = cols[: get_amount_of_columns(file, '\t') - 1]
    missing = list(set(cols).difference(use))
    df = pd.read_csv(file, sep='\t', names=use, engine='c')
    for entry in missing:
        df[entry] = None
    return df


def insert_tf(df, description):
    """
    Inserts bindingsites of a transcription factor into the database.
    :param df: Dataframe which should be inserted.
    :param description: Name of the TF.
    """
    if isinstance(df, pd.DataFrame):
        for idx, row in df.iterrows():
            # insert in site table
            insert_vals_s = list(row[SITE_INSERT])
            insert_vals_s.insert(0, description)
            print(insert_vals_s[:-1])
            sql = f"INSERT INTO site({SITE_COLUMNS}) VALUES(?,?,?,?,?);"
            c.execute(sql, insert_vals_s)

            # insert in bindingsite
            id = c.lastrowid
            insert_vals_bs = list(row[BINDING_INSERT])
            insert_vals_bs.insert(0, id)

            # calulation of old start and end for bindingsite table
            start = insert_vals_s[2]
            end = start + insert_vals_bs[5] + insert_vals_bs[6]
            start = start + insert_vals_bs[5] - insert_vals_bs[6]
            insert_vals_bs.pop(5)
            insert_vals_bs.pop(5)
            insert_vals_bs.append(start)
            insert_vals_bs.append(end)
            print(insert_vals_bs)
            c.execute(f"INSERT INTO bindingsite({BINDING_COLUMNS}) VALUES(?,?,?,?,?,?,?)", insert_vals_bs)

        conn.commit()
    else:
        raise ValueError("{} is not a Dataframe.".format(type(df)))


if __name__ == '__main__':
    for file in get_files('../../out/nuc/'):
        df = get_phylop_as_df(file, DF_COLUMNS)
        title = file.split('/')[-1].rstrip('_nuc_pp')
        print(title)
        insert_tf(df, title)
    conn.close()
