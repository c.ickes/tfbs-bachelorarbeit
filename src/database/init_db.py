import sqlite3 as sq

conn = sq.connect('../../data/database.db')
c = conn.cursor()


def init_database():
    '''
    Creates the database with all tables described in the ER diagram.
    '''
    c.execute(
        "CREATE TABLE IF NOT EXISTS site("
        "id integer primary key autoincrement, "
        "description text not null , "
        "chromosome text not null , "
        "start integer not null, "
        "end integer not null,"
        "phylop text)"
    )

    c.execute(
        "CREATE TABLE IF NOT EXISTS site_group("
        "name text primary key)"
    )

    c.execute(
        "CREATE TABLE IF NOT EXISTS bindingsite("
        "id integer primary key, "
        "sequence text,"
        "gen text, "
        "start integer,"
        "end integer, "
        "score real, "
        "FOREIGN KEY(id) REFERENCES site(id))"
    )

    c.execute(
        "CREATE TABLE IF NOT EXISTS belongs_to("
        "site_group text,"
        "site_id integer,"
        "label text, "
        "description text, "
        "PRIMARY KEY (site_group, site_id), "
        "FOREIGN KEY(site_group) REFERENCES site_group(name),"
        "FOREIGN KEY(site_id) REFERENCES site(id))"
    )

    conn.commit()


def init_index():
    """
    Creates the index on the database.
    """
    # index on chromosomes
    c.execute("CREATE INDEX IF NOT EXISTS chromosome_index ON site(chromosome)")
    # index on descriptions
    c.execute("CREATE INDEX IF NOT EXISTS description_index ON site(description)")
    # index on bindingsite
    c.execute("CREATE INDEX IF NOT EXISTS bindingsite_score ON bindingsite(score)")
    c.execute("CREATE INDEX IF NOT EXISTS bindingsite_gen ON bindingsite(gen)")
    c.execute("CREATE INDEX IF NOT EXISTS bindingsite_index ON bindingsite(chromosome, start, end)")
    conn.commit()


def init_view():
    """
    Creates a views on the database to preprocess the data for machine learning purpose.
    """
    c.execute("CREATE VIEW IF NOT EXISTS dataset "
              "AS "
              "SELECT "
              "belongs_to.label as label, "
              "site.phyloP as phylop, "
              "site.description as description, "
              "belongs_to.site_group as site_group, "
              "belongs_to.description as label_description "
              "FROM site INNER JOIN belongs_to "
              "ON site.id = belongs_to.site_id")

    c.execute("CREATE VIEW IF NOT EXISTS bindingsite_view "
              "AS "
              "SELECT "
              "site.id as id, "
              "bindingsite.chromosome as chromosome, "
              "bindingsite.start as bindingsite, "
              "bindingsite.end as end, "
              "bindingsite.score as score, "
              "bindingsite.gen as gen, "
              "site.description as factor "
              "FROM site INNER JOIN bindingsite "
              "ON site.id = bindingsite.id")

    conn.commit()


def optimize():
    """
    Optimizes the database by optimizing the statistics of the query optimizer.
    """
    c.execute("PRAGMA analysis_limit = 400;")
    c.execute("PRAGMA optimize;")

    c.execute("ANALYZE dataset")
    c.execute("ANALYZE belongs_to")
    c.execute("ANALYZE site")
    c.execute("ANALYZE bindingsite")


if __name__ == '__main__':
    #init_database()
    optimize()
    #init_index()
    #init_view()
    conn.close()
