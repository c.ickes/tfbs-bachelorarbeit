from sqlite3 import *
import numpy as np


def _create_label_matrix(num_classes):
    """
    Creates a N x N identity-matrix which allows to get the correct categorical label for a class by its row.
    """
    return np.eye(num_classes)


def _label_to_string(label_vec):
    """
    Converts a vector to comma separated values in a string.
    """
    return ''.join(f'{label},' for label in label_vec).rstrip(',')


class Labeler:
    def __init__(self, database_path, ignore_missings=False):
        """
        Connects to the database.
        """
        self.connection = connect(database_path)
        self.cursor = self.connection.cursor()
        self.ignor_missings = ignore_missings

    def _get_ids_on_percentile_per_tf(self, percentile, tf):
        self.cursor.execute(f"SELECT id, score "
                            f"FROM bindingsite_view "
                            f"WHERE factor = '{tf}'")
        result = self.cursor.fetchall()
        # raise an error if there is not suitable description

        if len(result) <= 0:
            raise Error(f"{tf} can not be found in the database.")

        result = np.array([list(res) for res in result])
        score = np.percentile(result[:, 1], percentile)
        result = result[result[:, 1] >= score]
        return result[:, 0].astype(int).flatten()

    def _get_ids(self, tfs, percentile):
        """
        Gathers the IDs of the bindingsites regarding a specific list of factors.
        If there is no a error will be raised.
        """
        tf_id_list = []
        for tf in tfs:
            try:
                result = self._get_ids_on_percentile_per_tf(percentile, tf)

                for id in result:
                    tf_id_list.append(id)

            except Error as e:
                if not self.ignor_missings:
                    raise e
                else:
                    print(e)

        return tf_id_list

    def _insert_into_belongs_to(self, class_label, description, tf_id_list, group_name):
        """
        Inserts a entry into the belongs_to tabel.
        A entry consists out of a groupname, a id of the tfb and the categorial label.
        """
        # convert array to string - to save it in the db
        class_label = _label_to_string(class_label)
        for id in tf_id_list:
            try:
                self.cursor.execute(f"INSERT INTO belongs_to(site_group, site_id, label, description) VALUES(?,?,?,?)",
                                    (group_name, int(id), class_label, description))

            except Error as e:
                raise e

    def _insert_into_site_group(self, group_name):
        """
        Inserts a group_name into the site_group table
        """
        self.cursor.execute("INSERT INTO site_group(name) VALUES(?)", (group_name,))

    def labeling(self, group_name, labels, score_percentile=0):
        """
        group_name - string
        labels - dict of lists, keys are holding descriptions
        """
        try:
            # create labels for each label
            self._insert_into_site_group(group_name)
            classes = _create_label_matrix(len(labels))
            for idx, val in enumerate(labels.items()):
                des, tfs = val
                tf_id_list = self._get_ids(tfs, score_percentile)
                self._insert_into_belongs_to(classes[idx], des, tf_id_list, group_name)

            self.connection.commit()
        except Error as e:
            self.connection.rollback()
            raise e

    def create_new_group(self, group_name):
        """
        Creates a new group according to the given group_name.
        If the group is already existing, False will be returned.
        """
        try:
            self.cursor(f"INSERT INTO site_group(name) VALUES(?)", (group_name,))
        except Error as e:
            raise e


    def close_connection(self):
        """
        Closes the connection to the database.
        """
        self.connection.close()
