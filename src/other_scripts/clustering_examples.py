import numpy as np
import numpy.random as npr
import sklearn.datasets as skd
from phylop_net.clusterer import _kmeans, _spectral_clustering_nn
import matplotlib.pyplot as plt
import os.path as osp

# This script creates the clustering example plots (from materials and methods)

# Plotting constants
COLORMAP = "RdBu"
POINTS = 200
SAVEPATH = "../out/decision_boundaries"


def create_samples(start, stop, label=1, f=lambda x: x):
    """
    Creates randomly points in a square which can be modified by the f function.
    Samples are defined on two axes (two dimensions).
    :param start: x, y start of possible generated samples
    :param stop: x, y, stop of possible generated samples
    :param label: class label two distinguish sample sets
    :param f: transformation function
    :return:    X_mat: sample matrix
                label_vec: class label vector
    """
    rand_idx = np.arange(0, POINTS)
    npr.shuffle(rand_idx)
    x_vec = np.linspace(start, stop, POINTS)

    y_vec = f(np.copy(x_vec))[rand_idx]
    X_mat = np.vstack((x_vec, y_vec)).T
    label_vec = np.ones(X_mat.shape[0]) * label
    return X_mat, label_vec


def sets():
    """
    Defines two sets. One where the distribution are not overlapping and one where the one class is nested in the other
    (dount shape)
    :return: generates each time the specific tuple
                first element: dataset
                second element: label vec
                third element: description
    """
    # set 1 (non overlapping)
    X_neg, y_neg = create_samples(-1, -0.1, label=-1)
    X_pos, y_pos = create_samples(0.1, 1)
    yield np.vstack((X_pos, X_neg)), np.hstack((y_pos, y_neg)), "non_overlapping"

    # set 2 (donut)
    X, y = skd.make_circles(POINTS * 2, factor=0.2, noise=0.1)
    yield X, y * 2 - 1, "donut"


def algorithms(X):
    """
    Generates the results of two different clustering approaches.
    (for kmeans and spectral clustering (nearest neighbor approach))
    :param X: dataset (two dimensions)
    :return: label vector of the specific algorithm
    """
    yield _kmeans(X, 2) * 2 - 1

    yield _spectral_clustering_nn(X, 2) * 2 - 1


def draw_plots(X, ax, row_idx):
    """
    Draws the clustered dataset in a scatterplot.
    :param X: dataset
    :param ax: axes of the predefined figure
    :param row_idx: defines the row axis.
    """
    ax[row_idx, 0].scatter(X[:, 0], X[:, 1], color='grey')

    for col_idx, y in enumerate(algorithms(X)):
        col_idx += 1
        ax[row_idx, col_idx].scatter(X[:, 0], X[:, 1], c=y, cmap=COLORMAP, vmin=-1.3, vmax=1.3)


# Generates the plot by using the defined functions.
# Datasets will be clustered by k-means and spectral clustering.
if __name__ == '__main__':
    plt.locator_params(nbins=4)
    fig, ax = plt.subplots(2, 3, sharex=True, sharey=True, figsize=(12, 8))
    plt.ylim([-1.1, 1.1])
    plt.xlim([-1.1, 1.1])
    for row_idx, vals in enumerate(sets()):
        X, y, label = vals
        draw_plots(X, ax, row_idx)

    ax[0, 0].set_title('Data', fontsize=20)
    ax[0, 1].set_title('KMeans', fontsize=20)
    ax[0, 2].set_title('Spectral Clustering', fontsize=20)
    fig.tight_layout()
    plt.savefig(osp.join(SAVEPATH, "clustering_examples.svg"))
