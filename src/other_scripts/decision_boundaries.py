import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import os.path as osp
import sklearn.datasets as skd
from sklearn.preprocessing import OneHotEncoder
import phylop_net.algorithms as al

# This script creates the decision boundary plots (from material and methods)

# Plotting constants
COLORMAP = "RdBu"
SAVEPATH = "../out/decision_boundaries"
POINTS = 50
SENSITY = 50


def create_samples(start, stop, label=1, f=lambda x: x):
    """
        Creates randomly points in a square which can be modified by the f function.
        Samples are defined on two axes (two dimensions).
        :param start: x, y start of possible generated samples
        :param stop: x, y, stop of possible generated samples
        :param label: class label two distinguish sample sets
        :param f: transformation function
        :return:    X_mat: sample matrix
                    label_vec: class label vector
    """
    rand_idx = np.arange(0, POINTS)
    npr.shuffle(rand_idx)
    x_vec = np.linspace(start, stop, POINTS)

    y_vec = f(np.copy(x_vec))[rand_idx]
    X_mat = np.vstack((x_vec, y_vec)).T
    label_vec = np.ones(X_mat.shape[0]) * label
    return X_mat, label_vec


def sets():
    """
    Generates four sets of points in a two dimensional space.
    1. Non overlapping
    2. one inner, one outer circle (dount shape)
    3. slightly overlapping
    4. completely overlapping
    :return: generates each time the specific tuple
                first element: dataset
                second element: label vec
                third element: description
    """
    # set 1 (non overlapping)
    X_neg, y_neg = create_samples(-1, -0.1, label=-1)
    X_pos, y_pos = create_samples(0.1, 1)
    yield np.vstack((X_pos, X_neg)), np.hstack((y_pos, y_neg)), "non_overlapping"

    # set 2 (donut)
    X, y = skd.make_circles(POINTS * 2, factor=0.3, noise=0.1)
    yield X, y * 2 - 1, "donut"

    # set 3 (slightly overlapping)
    X_neg, y_neg = create_samples(-1, 0.3, label=-1)
    X_pos, y_pos = create_samples(-0.3, 1)
    yield np.vstack((X_pos, X_neg)), np.hstack((y_pos, y_neg)), "slightly_overlapping"

    # set 4 (full overlapping)
    X_neg, y_neg = create_samples(-1, 1, label=-1)
    X_pos, y_pos = create_samples(-1, 1)
    yield np.vstack((X_pos, X_neg)), np.hstack((y_pos, y_neg)), "full_overlapping"


def algorithms(X, y, predict, predict2, label):
    """
    Defines the used algorithm for a classification.
    Used algorithms:
        1. Nearest Centroid
        2. Random Forest
        3. Neural Network
    Predicts the points from the dataset and also the decision boundary.
    :param X: dateset (feature matrix)
    :param y: class labels
    :param predict: first set to predict (points)
    :param predict2: secend set to predict (points)
    :param label: defined to overcome the centroid - bug
    :return:
    """
    cen = al.centroid()
    cen.fit(X, y)

    # Workaround to solve the bug with two equal mean vectors
    if label == "full_overlapping":
        yield np.ones(predict.shape[0])*-1, np.ones(predict2.shape[0])*-1, "centroid"
    else:
        yield cen.predict(predict), cen.predict(predict2), "centroid"
    print(cen.centroids_)

    rf = al.randomForest()
    rf.fit(X, y)
    yield rf.predict(predict), rf.predict(predict2), "RF"

    # relabeling to one hot encode
    ohe = OneHotEncoder(sparse=False)
    y_new = np.copy(y)
    y_new = y_new.reshape((y_new.shape[0], 1))
    yy = ohe.fit_transform(y_new)

    net = al.network(2, 2, 500, activation="sigmoid", loss="binary_crossentropy")
    net.fit(X, yy)
    net.evaluate(X, yy)
    yield np.argmax(net.predict(predict), axis=1)*2-1, np.argmax(net.predict(predict2), axis=1)*2-1, "MLP"


def draw_plots(X, y, label, ax, row_idx):
    """
    Draws the clustered dataset in a scatterplot and also visualizes the decision boundaries.
    :param X: dataset
    :param y: class labels
    :param ax: axes of the predefined figure
    :param row_idx: defines the row axis.
    """
    ax[row_idx, 0].scatter(X[:, 0], X[:, 1], c=y, cmap=COLORMAP, vmin=-1.3, vmax=1.3)

    space = get_space(X)
    for col_idx, vals in enumerate(algorithms(X, y, space, X, label)):
        space_pred, prediction, algo = vals
        col_idx += 1
        ax[row_idx, col_idx].scatter(X[:, 0], X[:, 1], c=y, cmap=COLORMAP, vmin=-1.3, vmax=1.3, edgecolors="black")
        ax[row_idx, col_idx].scatter(space[:, 0], space[:, 1], c=space_pred, cmap=COLORMAP, vmin=-1.3, vmax=1.3,
                                     alpha=0.1)


def get_space(X):
    """
    Generates a matrix of points from where the samples are located in. (for decision boundary)
    :param X: dataset to define the range.
    :return: Matrix of the whole space.
    """
    x = np.linspace(np.min(X[:, 0])-0.1, np.max(X[:, 0] + 0.1), SENSITY)
    y = np.linspace(np.min(X[:, 1])-0.1, np.max(X[:, 1] + 0.1), SENSITY)
    xx, yy = np.meshgrid(x, y)
    xx = np.ravel(xx)
    yy = np.ravel(yy)
    return np.vstack((xx, yy)).T


if __name__ == '__main__':
    plt.locator_params(nbins=4)
    fig, ax = plt.subplots(4, 4, sharex=True, sharey=True, figsize=(12, 8))
    plt.ylim([-1.1, 1.1])
    plt.xlim([-1.1, 1.1])
    for row_idx, vals in enumerate(sets()):
        X, y, label = vals
        draw_plots(X, y, label, ax, row_idx)

    ax[0, 0].set_title('Data', fontsize=20)
    ax[0, 1].set_title('Centroid', fontsize=20)
    ax[0, 2].set_title('RF', fontsize=20)
    ax[0, 3].set_title('MLP', fontsize=20)
    fig.tight_layout()
    plt.savefig(osp.join(SAVEPATH, "decision_boundaries.svg"))