from phylop_net import Visualizer
from database import Controller
from phylop_net.preprocessing import balancing

# Generates the balancing and data overview plots (from balancing in matrial and methods)
# superclass is used as site group

GROUP = 'superclass_filtered'
SAVEPATH = '../out/unsorted_results'

controller = Controller(GROUP)
X, y, labels = controller.get_data()

vis = Visualizer(X, labels, SAVEPATH, title=GROUP)
vis.get_pieplot()

X, y, labels = balancing(X, y, labels)

vis = Visualizer(X, labels, SAVEPATH, title=f"{GROUP}_balanced")
vis.get_pieplot()