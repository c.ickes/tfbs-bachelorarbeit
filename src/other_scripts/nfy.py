from database import Controller
from phylop_net import Visualizer
import matplotlib.pyplot as plt
import numpy as np

# Creating the plots for the discussion (just as an example)

SAVEPATH = "../out/unsorted_results"

con = Controller("superclass_filtered")
X, _, labels = con.get_data_per_tf("NFY_01")

vis = Visualizer(X, labels, SAVEPATH, "NFY")
vis.get_pca()
vis.get_tsne()

for res in X:
    plt.plot(np.arange(-73, 74), res, alpha=0.01, color="blue")
plt.savefig("../out/unsorted_results/NFY.svg")