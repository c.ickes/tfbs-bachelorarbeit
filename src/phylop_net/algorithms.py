from keras.models import Sequential
from keras.layers import Dense, Conv1D, AveragePooling1D, Flatten, SpatialDropout1D
from keras import activations, losses
import tensorflow as tf
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import NearestCentroid


# Contains methods for feature selection and classification.
# Predefines the configuration for the specific algorithm.

def randomForest():
    """
    :return: RF Classifier which uses 20 processes
    """
    randomF = RandomForestClassifier(n_jobs=20)
    return randomF


def centroid():
    """
    :return: Neareast Centroid classifier
    """
    centroid = NearestCentroid()
    return centroid


def network(classes, input_dim, hidden_layer_size, activation=activations.softmax, loss=losses.categorical_crossentropy):
    """
    Creates a neural network in the form of y(x)=Softmax(Relu(x^TW+b)W'+b') (Softmax can be changed)
    Hidden layer, input and output sizes have to be chosen before.
    :param classes: Amount of classes in the dataset
    :param input_dim: Amount of features of a sample
    :param hidden_layer_size: Number of neurons in the hidden layer
    :param activation: Last activiation function (can replace Softmax)
    :param loss: Loss function
    :return:
    """
    model = Sequential()
    model.add(Dense(hidden_layer_size, input_dim=input_dim))
    model.add(Dense(hidden_layer_size, activation=activations.relu))
    model.add(Dense(classes, activation=activation))
    model.compile(loss=loss, optimizer='adam', metrics=['accuracy', tf.keras.metrics.AUC(multi_label=True)])
    return model


def feature_selektion(X):
    """
    X - Input Matrix (2nd Dimension is important)
    returns each time the specified indices as an array
    """
    size = X.shape[1]
    mid = int(size / 2)
    widths = [15, 10, 5]
    # complete size
    yield "complete", np.arange(X.shape[1])

    # 31 from the middle
    for width in widths:
        yield f"middle:{width * 2 + 1}", np.arange(mid - width, mid + width + 1)

    # left from the center
    left = np.arange(mid - widths[0])
    yield "left", left

    # right from the center
    right = np.arange(mid + widths[0] + 1, size)
    yield "right", right

    # left and right from the center
    yield "outer", np.concatenate((left, right))
