import sklearn.cluster as skc
from sklearn.metrics import silhouette_score
from sklearn.preprocessing import LabelBinarizer
import numpy as np


def _kmeans(X, n_clusters):
    """
    Performs the clustering for the kmeans algorithm.
    :param X: dataset
    :param n_clusters: number of clusters
    :return: class labels for each sample in X
    """
    km = skc.KMeans(n_clusters=n_clusters)
    return km.fit_predict(X)


def _spectral_clustering_nn(X, n_clusters):
    """
    Performs the clustering for the spectral clustering algorithm, by using the nearest neighbor approach.
    :param X: dataset
    :param n_clusters: number of clusters
    :return: class labels for each sample in X
    """
    sc = skc.SpectralClustering(n_clusters=n_clusters, affinity="nearest_neighbors")
    return sc.fit_predict(X)


def _spectral_clustering_rbf(X, n_clusters):
    """
    Performs the clustering for the pectral clustering algorithm, by using the rbf approach (default).
    :param X: dataset
    :param n_clusters: number of clusters
    :return: class labels for each sample in X
    """
    sc = skc.SpectralClustering(n_clusters=n_clusters)
    return sc.fit_predict(X)


class Clusterer:
    def __init__(self, X):
        """
        :param X: dataset as Numpy matrix
        """
        self.X = X

    def _best_clustering(self, n_min, n_max, cluster_algorithm):
        """
       Evaluates the best amount of clusters by using a silhouette score. The cluster algorithm can be changed.
       :param n_min: Minimum number of clusters
       :param n_max: Maximum number of clusters
       :param cluster_algorithm: Specific cluster algorithm
       :return: labels and score with the best silhouett score
       """
        scores = []
        for i in range(n_min, n_max + 1):
            y = cluster_algorithm(self.X, i)
            scores.append(silhouette_score(self.X, y))

        labels = cluster_algorithm(self.X, np.argmax(scores) + n_min) + 1
        lb = LabelBinarizer()
        labels = lb.fit_transform(labels)

        if len(np.unique(labels, axis=0)) != labels.shape[0]:
            labels = np.hstack((labels, 1 - labels))

        return labels, scores

    def kmeans(self, n_min, n_max):
        """
        Performs the evaluation of the best number of clusters for the kmeans algorithm.
        :param n_min: Minimum number of clusters
        :param n_max: Maximum number of clusters
        :return: labels and score with the best silhouett score
        """
        return self._best_clustering(n_min, n_max, _kmeans)

    def spectral_clustering_nn(self, n_min, n_max):
        """
        Performs the evaluation of the best number of clusters for the spectral clustering algorithm
        with the nearest neighbor approach.
        :param n_min: Minimum number of clusters
        :param n_max: Maximum number of clusters
        :return: labels and score with the best silhouett score
        """
        return self._best_clustering(n_min, n_max, _spectral_clustering_nn)

    def spectral_clustering_rbf(self, n_min, n_max):
        """
        Performs the evaluation of the best number of clusters for the spectral clustering algorithm
        with the rbf approach.
        :param n_min: Minimum number of clusters
        :param n_max: Maximum number of clusters
        :return: labels and score with the best silhouett score
        """
        return self._best_clustering(n_min, n_max, _spectral_clustering_rbf)
