from sklearn.model_selection import train_test_split
import numpy as np

RANDOM_STATE = 42


def train_test(X, y=None):
    """
    Splits the dataset into training and test set. The test set will consists out of 1/3 from the original set.
    :param X: dataset
    :param y: class labels
    :return:
    """
    if y is not None:
        return train_test_split(X, y, test_size=0.33, random_state=RANDOM_STATE)
    else:
        return train_test_split(X, test_size=0.33, random_state=RANDOM_STATE)


def balancing(X, y, labels):
    """
       Balances the dataset and y labels. The minimum size of samples of a class is calculated first. Afterwards random subsets
       of this size are selected.
       :param X: data matrix
       :param y: class matrix
       :param labels: labels given by TFClass
       :return: balanced X, y, labels
    """
    np.random.seed(RANDOM_STATE)

    idx = np.arange(X.shape[0])
    np.random.shuffle(idx)

    X = X[idx]
    y = y[idx]
    labels = labels[idx]

    X_new, y_new, labels_new = None, None, None

    y_size = np.sum(y, axis=0)
    print(y_size)
    n_samples = int(np.min(y_size))
    print(f"New class-size: {n_samples}")

    for y_class in np.unique(y, axis=0):
        mask = np.all(y == y_class, axis=1)

        if X_new is None and y_new is None:
            X_new = X[mask][:n_samples, :]
            y_new = y[mask][:n_samples, :]
            labels_new = labels[mask][:n_samples]
        else:
            X_new = np.vstack((X_new, X[mask][:n_samples, :]))
            y_new = np.vstack((y_new, y[mask][:n_samples, :]))
            labels_new = np.hstack((labels_new, labels[mask][:n_samples]))

    return X_new, y_new, labels_new
