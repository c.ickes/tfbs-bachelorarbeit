import matplotlib.pyplot as plt
from os.path import join
import sklearn.decomposition as skd
import sklearn.manifold as skm
import numpy as np

RANDOM_STATE = 42


def _tsne(X):
    """
    Creates a TSNE transformed dataset by using a PCA initialization.
    :param X: dataset
    :return: TSNE transformed dataset
    """
    tsne = skm.TSNE(init='pca', early_exaggeration=30, learning_rate=400, n_jobs=5, random_state=RANDOM_STATE)
    return tsne.fit_transform(X)


def _pca(X):
    """
    Creates a PCA transformed dataset.
    :param X: dataset
    :return: PCA transformed dataset
    """
    pca = skd.PCA()
    return pca.fit_transform(X)

# Visualizes the data by using dimensionality reduction and mean plots
class Visualizer:
    def __init__(self, X, labels, savepath, title=""):
        """
        Predefines also markers and colors.
        :param X: dataset
        :param labels: class labels with the correct numbering
        :param savepath: path where the plots will be saved
        :param title: title prefix of plots
        """
        assert X.shape[0] == len(labels)
        self.X = X
        self.labels = labels
        self.savepath = savepath
        self.title = title
        self.colors = plt.cm.rainbow(np.linspace(0, 1, np.unique(labels).shape[0]))
        self.markers = ['o', 'v', 's', 'p', '*', 'X', 'D']

    def _create_plot(self, X, labels, title):
        """
        Creates a scatterplot for a transformed dataset.
        For the class level, marker shapes are added.
        Plots will be saved in the SAVEPATH directory.
        :param X: transformed dataset (two dimensions)
        :param labels: real class labels (not one-hot encoded)
        :param title: title of the figure
        """
        fig, ax = plt.subplots(figsize=(12, 8))
        for color_idx, label in enumerate(np.unique(labels)):
            if len(label.split('.')) > 1:
                marker = self.markers[int(label.split('.')[1])-1]
            else:
                marker = 'o'

            mask = (labels == label)
            plt.scatter(X[mask, 0], X[mask, 1], alpha=0.7, marker=marker, label=label, color=self.colors[color_idx])

        plt.title(title)
        plt.legend()
        plt.tight_layout()
        plt.savefig(join(self.savepath, f"{title}.svg"))
        plt.close()

    def _create_bpplot(self, prefer):
        """
        Creates the bpplot with separate highlighted classes.
        Saves the plot directly.
        :param prefer: Highlighted class
        """
        fig, ax = plt.subplots(figsize=(12, 8))
        # create x and y axis
        x = np.arange(0, self.X.shape[1]) - (int(self.X.shape[1] / 2) + 1)

        for color_idx, label in enumerate(np.unique(self.labels)):
            ys = np.mean(self.X[self.labels == label], 0)
            if label in prefer:
                plt.plot(x, ys, label=label, c=self.colors[color_idx])
            else:
                plt.plot(x, ys, alpha=0.05, c=self.colors[color_idx])

        plt.title(f"{self.title}-BPPlot")
        plt.legend()
        plt.tight_layout()
        plt.savefig(join(self.savepath, f"{self.title}-{prefer}-BPPlot.svg"))
        plt.close()

    def _create_pie_plot(self, patches, labels, suffix):
        """
        Creates the basic pie plot and saves it directly.
        :param patches: patch sizes
        :param labels: list of labels for each patch
        :param suffix: title suffix
        """
        plt.legend(patches, labels, loc='center left', bbox_to_anchor=(-0.1, 1.))
        plt.title(f"{self.title} - AmountVectors")
        plt.tight_layout()
        plt.savefig(join(self.savepath, f"{self.title}-AmountVectors_{suffix}.svg"))
        plt.close()

    def get_pca(self):
        """
        Transforms the dataset by using PCA and creates a scatterplot.
        Saves the plot directly.
        """
        X = _pca(self.X)
        self._create_plot(X, self.labels, f'{self.title}-PCA')

    def get_tsne(self):
        """
        Transforms the dataset by using TSNE and creates a scatterplot.
        Saves the plot directly.
        """
        X = _tsne(self.X)
        self._create_plot(X, self.labels, f'{self.title}-TSNE')

    def get_bpplot(self):
        """
        Creates mean plots for all classes
        """
        y = self.labels
        for label in np.unique(y):
            self._create_bpplot([label])

        self._create_bpplot(np.unique(y))

    def get_pieplot(self):
        """
        Creates the specifc pie plots for percentage and number of samples.
        """
        fig, ax = plt.subplots(figsize=(12, 8))
        labels_per, label_amount, sizes = [], [], []
        for label in np.unique(self.labels):
            amount = np.sum(self.labels == label)
            size = amount / len(self.labels)
            sizes.append(size)
            labels_per.append("{} - {:1.2f} %".format(label, size * 100))
            label_amount.append("{} - {}".format(label, amount))

        patches, texts = plt.pie(sizes)
        self._create_pie_plot(patches, labels_per, "percentage")
        patches, texts = plt.pie(sizes)
        self._create_pie_plot(patches, label_amount, "amount")


