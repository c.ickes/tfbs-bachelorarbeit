import tensorflow as tf
import numpy as np
from database import ClusterController
from phylop_net.algorithms import feature_selektion
from phylop_net import Visualizer

# Creates the TSNE and PCA plots based on the best predicted results
# classifying.py has to run before, otherwise the models can not be found.

MODEL_PATH = "../out/models"


def remap(mapping, array):
    """
    Remaps the TFClass classes to the one-hot encodings
    :param mapping: mapping of TFClass
    :param array: one-hot encodings
    :return: Array of classes (from TFClass)
    """
    labels = []
    for i in range(len(array)):
        labels.append(mapping[array[i]])

    return np.array(labels)


def load_model(model_name):
    """
    Loads the specific trained model.
    :param model_name: Name of the model
    :return: loaded h5 file
    """
    return tf.keras.models.load_model(f"{MODEL_PATH}/model_{model_name}.h5", compile=False)


if __name__ == '__main__':
    group = "class_filtered"

    model = load_model("superclass_filtered_middle:21")

    con_clus = ClusterController(group)
    X_tf, y_tf, labels_tf = con_clus.get_data()
    idx_list = list(feature_selektion(X_tf))
    X_tf = X_tf[:, idx_list[2][1]]

    # create mapping
    y_tf = np.argmax(y_tf, axis=1)
    _, unique_idx = np.unique(labels_tf, return_index=True)
    mapping = {y_tf[idx]: labels_tf[idx] for idx in unique_idx}

    y_tf_pred = np.argmax(model.predict(X_tf), axis=1)
    labels_tf_pred = remap(mapping, y_tf_pred)
    print(X_tf)

    vis = Visualizer(X_tf, labels_tf_pred, "../out/unsorted_results", f"{group}_predicted")
    vis.get_pca()
    vis.get_tsne()
